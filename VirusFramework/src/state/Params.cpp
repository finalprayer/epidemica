#include "epidemica/state/Params.h"
#include "epidemica/helper/RapidJSONHelper.h"

namespace Epidemica
{
    Epidemica::Params::Params() {}

    Epidemica::Params::~Params() {}

#if BOOST_FOUND
    Epidemica::Params::Params(const boost::property_tree::ptree& params)
    {
        vectorDiffusionProbability = params.get<double>("vector-diffusion-probability");
        bitingRate = params.get<double>("biting-rate");
        vectorInfectionProbability = params.get<double>("vector-infection-probability");
        hostInfectionProbability = params.get<double>("host-infection-probability");
        vectorEIRate = params.get<double>("vector-e-to-i-rate");
        hostEIRate = params.get<double>("host-e-to-i-rate");
        hostIRRate = params.get<double>("host-i-to-r-rate");
        vectorBirthRate = params.get<double>("vector-birth-rate");
        vectorMaturationRate = params.get<double>("vector-maturation-rate");
        vectorDeathRate = params.get<double>("vector-death-rate");
    }
#endif

#if RAPIDJSON_FOUND
    Params::Params(const rapidjson::GenericObject<true, rapidjson::Value>& params)
    {
        Load(params);
    }

    void Params::Load(const rapidjson::GenericObject<true, rapidjson::Value>& params)
    {
        vectorDiffusionProbability = RapidJSONHelper::ParseValueOrDefault<double>(params, "vector-diffusion-probability", 0);
        bitingRate = RapidJSONHelper::ParseValueOrDefault<double>(params, "biting-rate", 0);
        vectorInfectionProbability = RapidJSONHelper::ParseValueOrDefault<double>(params, "vector-infection-probability", 0);
        hostInfectionProbability = RapidJSONHelper::ParseValueOrDefault<double>(params, "host-infection-probability", 0);
        vectorEIRate = RapidJSONHelper::ParseValueOrDefault<double>(params, "vector-e-to-i-rate", 0);
        hostEIRate = RapidJSONHelper::ParseValueOrDefault<double>(params, "host-e-to-i-rate", 0);
        hostIRRate = RapidJSONHelper::ParseValueOrDefault<double>(params, "host-i-to-r-rate", 0);
        vectorBirthRate = RapidJSONHelper::ParseValueOrDefault<double>(params, "vector-birth-rate", 0);
        vectorMaturationRate = RapidJSONHelper::ParseValueOrDefault<double>(params, "vector-maturation-rate", 0);
        vectorDeathRate = RapidJSONHelper::ParseValueOrDefault<double>(params, "vector-death-rate", 0);
    }

    void Params::Save(const rapidjson::GenericObject<false, rapidjson::Value>& params, rapidjson::MemoryPoolAllocator<>& allocator) const
    {
        params["vector-diffusion-probability"].Set<double>(vectorDiffusionProbability);
        params["biting-rate"].Set<double>(bitingRate);
        params["vector-infection-probability"].Set<double>(vectorInfectionProbability);
        params["host-infection-probability"].Set<double>(hostInfectionProbability);
        params["vector-e-to-i-rate"].Set<double>(vectorEIRate);
        params["host-e-to-i-rate"].Set<double>(hostEIRate);
        params["host-i-to-r-rate"].Set<double>(hostIRRate);
        params["vector-birth-rate"].Set<double>(vectorBirthRate);
        params["vector-maturation-rate"].Set<double>(vectorMaturationRate);
        params["vector-death-rate"].Set<double>(vectorDeathRate);
    }
#endif
}