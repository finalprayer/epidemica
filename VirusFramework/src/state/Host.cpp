#include "stdafx.h"

#include "epidemica/state/Host.h"
#if MPI_FOUND
#include <mpi.h>
#endif

Epidemica::Host::Host()
{
}

Epidemica::Host::Host(int hostId, int locationId, InfectionState infectionState, int homeId, int hubId) :
    m_hostId(hostId),
    m_locationId(locationId),
    m_infectionState(infectionState),
    m_homeId(homeId),
    m_hubId(hubId)
{
}

Epidemica::Host::~Host()
{
}

#if BOOST_FOUND
Epidemica::Host::Host(int hostId, const boost::property_tree::ptree& host)
{
    m_hostId = hostId;
    m_locationId = host.get<int>("location");
    m_infectionState = Epidemica::StringToInfectionState((host.get<char>("i-state"));
    m_homeId = host.get<int>("home");
    m_hubId = host.get<int>("hub");
}
#endif

#if RAPIDJSON_FOUND
Epidemica::Host::Host(int hostId, const GenericObject& host)
{
    m_hostId = hostId;
    m_locationId = host["location"].GetInt();
    m_infectionState = Epidemica::StringToInfectionState(host["i-state"].GetString());
    m_homeId = host["home"].GetInt();
    m_hubId = host["hub"].GetInt();
}
#endif

#if MPI_FOUND
//Currently not used, but is required for sending structs via MPI
bool Epidemica::Host::s_mpi_initialized = false;
MPI_Datatype Epidemica::Host::s_mpi_datatype = 0;

MPI_Datatype Epidemica::Host::GetMPIDatatype()
{
    if (!s_mpi_initialized)
    {
        InitMPIDatatype();
    }
    return s_mpi_datatype;
}

void Epidemica::Host::InitMPIDatatype()
{
    const int ELEMENTS = 5;

    if (Host::s_mpi_datatype != 0)
		throw std::runtime_error("mpi host already initialized");

    MPI_Datatype types[ELEMENTS] =
    {
        MPI_INT, //host_id
        MPI_INT, //location_id
        MPI_INT, //infectionState
        MPI_INT, //homeId
        MPI_INT,  //hubId
    };

    int blocklengths[ELEMENTS] =
    {
        1,
        1,
        1,
        1,
        1,
    };

    MPI_Aint offsets[ELEMENTS] =
    {
        offsetof(struct Host, m_hostId),
        offsetof(struct Host, m_locationId),
        offsetof(struct Host, m_infectionState),
        offsetof(struct Host, m_homeId),
        offsetof(struct Host, m_hubId)
    };

    MPI_Type_create_struct(ELEMENTS, blocklengths, offsets, types, &Host::s_mpi_datatype);
    MPI_Type_commit(&Host::s_mpi_datatype);
}
#endif