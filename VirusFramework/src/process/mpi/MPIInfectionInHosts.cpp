#include "stdafx.h"
#include "epidemica/process/mpi/MPIInfectionInHosts.h"
#include "epidemica/state/SimulationState.h"

#include <cmath>

Epidemica::MPI::MPIInfectionInHosts::MPIInfectionInHosts()
{
    m_generator = std::default_random_engine();
}

Epidemica::MPI::MPIInfectionInHosts::~MPIInfectionInHosts()
{
}

std::string Epidemica::MPI::MPIInfectionInHosts::GetName() const
{
    return "MPI::MPIInfectionInHosts";
}

void Epidemica::MPI::MPIInfectionInHosts::Execute(SimulationState& state)
{
    /*
    Original Python
    eToIRate = params['host-e-to-i-rate'] # see[1]
    iToRRate = params['host-i-to-r-rate'] # see[1]
    pITrans = 1.0 - math.exp(-eToIRate)
    pRTrans = 1.0 - math.exp(-iToRRate)
    for iid in hosts :
        h = hosts[iid]
        s = h['i-state']
        if s == 'E' :
            if binSamp(1, pITrans) : h['i-state'] = 'I'
        elif s == 'I' :
            if binSamp(1, pRTrans) : h['i-state'] = 'R'
    */

    double pITrans = 1.0f - std::exp(-state.m_params.hostEIRate);
    double pRTrans = 1.0f - std::exp(-state.m_params.hostIRRate);

    for (unsigned int i = 0; i < state.m_hosts.size(); i++)
    {
        if (state.m_hosts[i].m_infectionState == InfectionState::E)
        {
            std::binomial_distribution<int> binomial(1, pITrans);
            //TODO: shouldnt need a binomial sample here, bernoulli trial should be fine
            int sample = binomial(m_generator);
            if(sample) state.m_hosts[i].m_infectionState = InfectionState::I;
        }

        else if (state.m_hosts[i].m_infectionState == InfectionState::I)
        {
            std::binomial_distribution<int> binomial(1, pRTrans);
            //TODO: shouldnt need a binomial sample, bernoulli trial should be fine
            int sample = binomial(m_generator);
            if(sample) state.m_hosts[i].m_infectionState = InfectionState::R;
        }
    }

}
