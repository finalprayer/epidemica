#ifdef OPENMP_FOUND
#include "stdafx.h"
#include "epidemica/process/omp/OMPHostInfection.h"
#include "epidemica/state/SimulationState.h"

#include <easylogging++.h>
#include <omp.h>

#if MPI_FOUND
#include <mpi.h>
#endif

Epidemica::OMP::OMPHostInfection::OMPHostInfection()
{
#if MPI_FOUND
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    m_generator = random_engine((unsigned int)rank);
#else
    m_generator = random_engine();
#endif
}

Epidemica::OMP::OMPHostInfection::~OMPHostInfection()
{
}

std::string Epidemica::OMP::OMPHostInfection::GetName() const
{
    return "OMP::OMPHostInfection";
}

void Epidemica::OMP::OMPHostInfection::Execute(SimulationState& state)
{
    double bitingRate = state.m_params.bitingRate;
    double infectionProbability = state.m_params.hostInfectionProbability;

    #pragma omp parallel for
    for (int i = 0; i < (int)state.m_locations.size(); i++)
    {
        Location& location = state.m_locations[i];
        int infected = location.m_vectorState.SEIR[InfectionState::I];
        if (infected == 0) continue;

        int totalHosts = 0;
        int susceptableHosts = 0;

        for (int hostId : location.m_hosts)
        {
            if (state.m_hosts[hostId].m_infectionState == InfectionState::S)
            {
                susceptableHosts++;
            }
            totalHosts++;
        }

        if (susceptableHosts == 0) continue;

        int totalVectors = 0;
        for (int i = 0; i < INFECTION_STATES; i++)
        {
            totalVectors += location.m_vectorState.SEIR[i];
        }

        double beta = bitingRate * infectionProbability * (infected / totalHosts);
        double p = 1.0 - std::exp(-beta);

        for (int hostId : location.m_hosts)
        {
            Host& host = state.m_hosts[hostId];
            if (host.m_infectionState != InfectionState::S) continue;

            std::binomial_distribution<int> binomial(1, p);
            if (binomial(m_generator))
            {
                host.m_infectionState = InfectionState::E;
                //LOG(INFO) << state.m_config.CurrentCycle << " : infection" << std::endl;
                
            }

        }
    }
}
#endif