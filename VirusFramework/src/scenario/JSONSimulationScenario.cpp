#if RAPIDJSON_FOUND

#include "epidemica/scenario/JSONSimulationScenario.h"
#include "epidemica/state/SimulationState.h"
#include "epidemica/helper/RapidJSONHelper.h"
#if _DEBUG
#include <rapidjson/prettywriter.h>
#endif
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>

#include <sstream>

//C++17
//#include <filesystem>

#include <memory>
#include <string>
#include <istream>
#include <fstream>
#include <cstdio>

//for datetime
#include <iostream>
#include <iomanip>
#include <ctime>


#if __GNUC__
#include <sys/stat.h>
#endif

/**
 * Size of the input buffer to read JSON files.
 * NOTE: larger values may improve performance.
 */
static int MAX_DECIMAL_PRECISION = 2;
static const int INPUT_BUFFER_SIZE = 65536;
static const char* READ_FLAGS =
#if WIN32
"rb";
#else
"r";
#endif
static const char* WRITE_FLAGS =
#if WIN32
"wb";
#else
"w";
#endif


inline std::string ParentPath(const std::string& filepath)
{
#if BOOST_FOUND
    #include <boost/filesystem/path.hpp>
    return boost::filesystem::path(m_configfile).parent_path().generic_string();
#else
    size_t found;
    found = filepath.find_last_of("/\\");
    if (found == std::string::npos) return "";
    else return filepath.substr(0, found);
#endif
}

inline bool FileExists(const std::string& filepath)
{
    //TODO: ifstream implementation on magnus has issues use posix functions in the meantime
#if __GNUC__
    struct stat buffer;
    return (stat(filepath.c_str(), &buffer) == 0);
#else
    std::ifstream file = std::ifstream(filepath);
    return file.good();
#endif
}


namespace Epidemica
{

    JSONSimulationScenario::JSONSimulationScenario()
    {
    }

    JSONSimulationScenario::JSONSimulationScenario(const std::string& configpath)
    {
        LoadScenarioFromFile(configpath);
    }

    const std::string& JSONSimulationScenario::GetFileLocation()
    {
        return m_configfile;
    }

    const rapidjson::Document& JSONSimulationScenario::GetConfig() const
    {
        return *m_config.get();
    }
    const rapidjson::Document& JSONSimulationScenario::GetHosts() const
    {
        return *m_hosts.get();
    }
    const rapidjson::Document& JSONSimulationScenario::GetLocations() const
    {
        return *m_locations.get();
    }
    const rapidjson::Document& JSONSimulationScenario::GetParams() const
    {
        return *m_params.get();
    }

    void JSONSimulationScenario::LoadScenarioFromFile(const std::string& configpath)
    {
        m_configfile = configpath;
        LoadScenarioFromFile();
    }

    void JSONSimulationScenario::LoadScenarioFromFile()
    {
        std::string directory = ParentPath(m_configfile);

        //Config
        m_config.reset(new rapidjson::Document());
        try
        {
            LoadDocument(m_configfile, *m_config);
        }
        catch (std::exception&)
        {
            std::stringstream s;
            s << "failed to read : " << m_configfile;
            throw std::invalid_argument(s.str());
        }

        //Config Chaining
        if (m_config->HasMember("config-file"))
        {
            if (!GetConfig()["config-file"].IsString())
            {
                throw std::invalid_argument("config-file must be a string");
            }
            LoadScenarioFromFile(GetConfig()["config-file"].GetString());
            return;
        }

        //Params
        m_params.reset(new rapidjson::Document());
        try
        {
            LoadDocumentFallback("params-file", "params.json", directory, m_paramsfile, *m_params);
        }
        catch (std::exception&)
        {
            //Support reading original json layout by combining config and params
            m_params->CopyFrom(*m_config, m_params->GetAllocator());
            //Write out to new stylev
            m_configfile = directory + "/config.json";
        }

        //Hosts
        m_hosts.reset(new rapidjson::Document());
        LoadDocumentFallback("hosts-file", "hosts.json", directory, m_hostsfile, *m_hosts);

        //Locations
        m_locations.reset(new rapidjson::Document());
        LoadDocumentFallback("locations-file", "locations.json", directory, m_locationsfile, *m_locations);
    }

    void JSONSimulationScenario::SaveScenarioToInputFile()
    {
        SaveDocument(m_configfile, GetConfig());
        SaveDocument(m_paramsfile, GetParams());
        SaveDocument(m_hostsfile, GetHosts());
        SaveDocument(m_locationsfile, GetLocations());
    }

    void JSONSimulationScenario::SaveScenarioToFile(const std::string& fileprefix, const std::string& path)
    {
        //throw new std::runtime_error("not implemented");

        SaveScenarioToFile(
            fileprefix + ".config.json", 
            fileprefix + ".params.json",
            fileprefix + ".hosts.json",
            fileprefix + ".locations.json");

    }

    void JSONSimulationScenario::SaveScenarioToFile(
        const std::string& configpath,
        const std::string& paramspath,
        const std::string& hostspath,
        const std::string& locationspath)
    {
        std::string tconfig = m_configfile;
        std::string tparams = m_paramsfile;
        std::string thosts = m_hostsfile;
        std::string tlocations = m_locationsfile;

        //Update the rapidjson doc such that the backup points to
        // the correct files.
        UpdateConfigPaths(paramspath, hostspath, locationspath);

        SaveDocument(configpath, GetConfig()); //Must use this not m_configpath
        SaveDocument(paramspath, GetParams());
        SaveDocument(hostspath, GetHosts());
        SaveDocument(locationspath, GetLocations());

        //point the rapidjson docs back to the currently loaded files.
        UpdateConfigPaths(tparams, thosts, tlocations);
    }

    void JSONSimulationScenario::SaveScenarioToCheckpointFile()
    {
        //Get time
        time_t t = std::time(nullptr);
#if WIN32
        tm time;
        errno_t error = localtime_s(&time, &t);
        if (error) throw std::runtime_error("failed to get local time");
#else
        tm time = *std::localtime(&t);
#endif

        std::string configCheckpointPath = AppendCheckpointStamp(time, m_configfile);
        std::string paramsCheckpointPath = AppendCheckpointStamp(time, m_paramsfile);
        std::string locationsCheckpointPath = AppendCheckpointStamp(time, m_locationsfile);
        std::string hostsCheckpointPath = AppendCheckpointStamp(time, m_hostsfile);

        SaveScenarioToFile(
            configCheckpointPath,
            paramsCheckpointPath,
            hostsCheckpointPath,
            locationsCheckpointPath);

    }

    void JSONSimulationScenario::LoadDocumentFallback(
        const char* key,
        const char* defaultname,
        const std::string& directory,
        std::string& file,
        rapidjson::Document& doc)
    {
        if (!GetConfig().HasMember(key))
        {
            throw std::invalid_argument("config does not contain " + std::string(key));
        }

        //try querying a custom file path
        file = GetConfig()[key].IsString() ? GetConfig()[key].GetString() : defaultname;
        if (!FileExists(file)) //try relative path to working directing
        {
            file = directory + "/" + file;
        }
        if (!FileExists(file)) //try relative path to config
        {
            throw std::invalid_argument("file not found : " + file);
        }

        try //load existing document
        {
            LoadDocument(file, doc);
        }
        catch (std::exception& ex)
        {
            std::cerr << "failed to read : " << file << std::endl;
            throw ex;
        }
    }

    void JSONSimulationScenario::LoadDocument(const std::string& filepath, rapidjson::Document& doc)
    {
        //Ifstream approach
        //std::ifstream stream(filepath);
        //doc.ParseStream(IStreamWrapper(stream));

        //std::ifstream is slower than FileReadStream as FILE* grants control 
        // over the buffer length. see http://rapidjson.org/md_doc_stream.html

        FILE* fp;
#if WIN32
        int error = fopen_s(&fp, filepath.c_str(), READ_FLAGS);
        if (error) throw std::runtime_error("Failed to open file at : " + filepath);
#else
        fp = fopen(filepath.c_str(), READ_FLAGS);
        if (fp == NULL)
        {
            throw std::runtime_error("Failed to open file at : " + filepath);
        }
#endif
        std::vector<char> readBuffer = std::vector<char>(INPUT_BUFFER_SIZE);
        rapidjson::FileReadStream stream(fp, readBuffer.data(), readBuffer.capacity() * sizeof(char));
        doc.ParseStream(stream);
        fclose(fp);
        rapidjson::ParseErrorCode parseError = doc.GetParseError();
        if (parseError)
        {
            std::stringstream ss;
            ss << "json parsing error when loading: " << filepath;
            throw std::runtime_error(ss.str());
        }

    }

    void JSONSimulationScenario::SaveDocument(const std::string& filepath, const rapidjson::Document& doc)
    {
        FILE* fp;
#if WIN32
        int error = fopen_s(&fp, filepath.c_str(), WRITE_FLAGS);
        if (error) throw std::runtime_error("Failed to open file for write at : " + filepath);
#else
        fp = fopen(filepath.c_str(), WRITE_FLAGS);
        if (fp == NULL)
        {
            throw std::runtime_error("failed to open file for read at " + filepath);
        }
#endif
        std::vector<char> writeBuffer = std::vector<char>(INPUT_BUFFER_SIZE);
        rapidjson::FileWriteStream stream(fp, writeBuffer.data(), writeBuffer.capacity() * sizeof(char));

#if _DEBUG
        rapidjson::PrettyWriter<rapidjson::FileWriteStream> writer(stream);
#else
        rapidjson::Writer<rapidjson::FileWriteStream> writer(stream);
#endif
        //writer.SetMaxDecimalPlaces(MAX_DECIMAL_PRECISION);
        doc.Accept(writer);
        fclose(fp);
    }

    void JSONSimulationScenario::LoadState(SimulationState& state) const
    {

        //Config
        state.m_config = Config(GetConfig().GetObject());

        //Params
        state.m_params = Params(GetParams().GetObject());


        //Data
        //TODO: consider simplfying to this
        //state.m_data = Data(m_locations, m_hosts);

        //Locations
        state.m_locations = vector<Location>(m_locations->MemberCount());
        int i = 0;
        for (auto it = GetLocations().MemberBegin(); it != m_locations->MemberEnd(); it++)
        {
            int id = atoi(it->name.GetString());
            state.m_locations[i] = Location(id, it->value.GetObject());
            i++;
        }

        //Hosts
        state.m_hosts = vector<Host>(GetHosts().MemberCount());
        i = 0;
        for (auto it = GetHosts().MemberBegin(); it != m_hosts->MemberEnd(); it++)
        {
            int id = atoi(it->name.GetString());
            state.m_hosts[i] = Host(id, it->value.GetObject());
            i++;
        }
    }

    void JSONSimulationScenario::SaveState(const SimulationState& state)
    {
        //config and simulation
        state.m_config.Save(m_config->GetObject(), m_config->GetAllocator());

        //params
        state.m_params.Save(m_params->GetObject(), m_params->GetAllocator());

        //locations
        //state.m_locations.Save(m_locations->GetObject(), m_locations->GetAllocator());
        RapidJSONHelper::SaveLocations(state.m_locations, m_locations->GetObject(), m_locations->GetAllocator());

        //hosts
        //state.m_hosts.Save(m_hosts->GetObject(), m_host->GetAllocator());
        RapidJSONHelper::SaveHosts(state.m_hosts, m_hosts->GetObject(), m_hosts->GetAllocator());

    }

    bool JSONSimulationScenario::Compare(const JSONSimulationScenario& other) const
    {
        return (*m_params) == (*other.m_params) 
            && (*m_hosts) == (*other.m_hosts)
            && (*m_locations) == (*other.m_locations);
    }
   
    void JSONSimulationScenario::UpdateConfigPaths(
        const std::string& paramspath,
        const std::string& hostspath,
        const std::string& locationspath)
    {
        (*m_config)["params-file"].SetString(paramspath.c_str(), (rapidjson::SizeType)paramspath.length(), m_config->GetAllocator());
        (*m_config)["hosts-file"].SetString(hostspath.c_str(), (rapidjson::SizeType)hostspath.length(), m_config->GetAllocator());
        (*m_config)["locations-file"].SetString(locationspath.c_str(), (rapidjson::SizeType)locationspath.length(), m_config->GetAllocator());
    }

    std::string JSONSimulationScenario::AppendCheckpointStamp(const tm& time, const std::string& filepath)
    {
        const char* format = "%Y%m%d-%H%M%S";

#if __GNUC__
        char formattedTime[100];
        std::strftime(formattedTime, 100, format, &time);
#else
        auto& formattedTime = std::put_time(&time, format);
#endif

        size_t splitIndex = filepath.find_last_of("/") + 1;

        std::stringstream filenamestream;
        filenamestream << filepath.substr(0, splitIndex)
            << formattedTime
            //<< "_cycle" << RapidJSONHelper::ParseValue<int>(GetConfig().GetObject(), "current-cycle")
            << "." << filepath.substr(splitIndex, filepath.length()-1);

        return filenamestream.str();
    }
}
#endif
