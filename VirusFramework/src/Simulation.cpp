#include "stdafx.h"

#include "Globals.h"

#include "epidemica/Simulation.h"
#include "epidemica/scenario/ScenarioFactory.h"
#include "epidemica/scenario/ISimulationScenario.h"
#include "epidemica/state/SimulationState.h"
#include "epidemica/DeviceManager.h"
#include "epidemica/process/ProcessPipeline.h"
#include "epidemica/process/AbstractProcessFactory.h"

namespace Epidemica
{
#pragma region Constructors
    Epidemica::Simulation::Simulation(
        const std::string& factoryTag, 
        const std::string& scenarioFile)
    {
        m_deviceManager = std::make_shared<DeviceManager>();

        m_scenario.reset(ScenarioFactory::MakeDynamicSimulationScenarioFromJSONFile(scenarioFile));
        m_state.reset(new SimulationState(*m_scenario.get()));

        m_processes = std::shared_ptr<ProcessPipeline>(m_deviceManager->CreateArbovirusProcessPipeline(factoryTag));
    }

    Epidemica::Simulation::Simulation(
        const std::string&  factoryTag,
        std::shared_ptr<DeviceManager> deviceManager)
    {
        m_deviceManager = deviceManager;
        m_processes = std::shared_ptr<ProcessPipeline>(m_deviceManager->CreateArbovirusProcessPipeline(factoryTag));
    }

    Epidemica::Simulation::Simulation(
        const std::string& factoryTag,
        std::shared_ptr<DeviceManager> deviceManager, 
        std::shared_ptr<ISimulationScenario> scenario)
        : Simulation(
            factoryTag,
            deviceManager,
            scenario,
            std::shared_ptr<ProcessPipeline>(deviceManager->CreateArbovirusProcessPipeline(factoryTag)))
    {
    }

    Epidemica::Simulation::Simulation(
        const std::string&  factoryTag,
        std::shared_ptr<DeviceManager> deviceManager,
        std::shared_ptr<ISimulationScenario> scenario,
        std::shared_ptr<ProcessPipeline> processPipeline)
    {
        m_deviceManager = deviceManager;
        m_scenario = scenario;
        m_processes = std::move(processPipeline);
    }

    Epidemica::Simulation::~Simulation()
    {
    }

#pragma endregion Constructor

#pragma region Accessor_Mutators

    bool Epidemica::Simulation::IsDone() const
    {
        //return m_currentStep > m_totalSteps;
        return GetState().m_config.CurrentCycle >= GetState().m_config.TotalCycles;
    }

    bool Epidemica::Simulation::IsCheckpoint() const
    {
        return m_state->m_config.CheckpointInterval > 0
            && m_state->m_config.CurrentCycle % m_state->m_config.CheckpointInterval == 0;
    }

    int Epidemica::Simulation::GetCheckpointInterval() const
    {
        return GetState().m_config.CheckpointInterval;
    }

    void Epidemica::Simulation::SetCheckpointInterval(int interval)
    {
        GetState().m_config.CheckpointInterval = interval;
    }

    void Epidemica::Simulation::ResetSteps(int currentStep, int totalSteps)
    {
        //reserves space for the tracking vector
        m_hostsEpidemic.clear();
        m_hostsEpidemic.reserve(totalSteps);
        GetState().m_config.CurrentCycle = currentStep;
        GetState().m_config.TotalCycles = totalSteps;
    }

#pragma endregion Accessor_Mutators

#pragma region Scenario_Methods

    void Epidemica::Simulation::LoadStateFromScenario()
    {
        m_state.reset(new SimulationState(*m_scenario.get()));
    }

    void Epidemica::Simulation::SaveStateToScenario()
    {
        m_scenario->SaveState(*m_state);
    }

    void Epidemica::Simulation::SaveScenarioToInputFile()
    {
        m_scenario->SaveScenarioToInputFile();
    }

    void Epidemica::Simulation::SaveScenarioToCheckpointFile()
    {
        m_scenario->SaveScenarioToCheckpointFile();
    }

#pragma endregion ScenarioMethods

    void Epidemica::Simulation::Initialize()
    {
        //TODO:
        //m_activeFactory->Initialize();
    }

    void Epidemica::Simulation::Run()
    {
        while (m_state->m_config.CurrentCycle < m_state->m_config.TotalCycles)
        {
            Step();
        }
    }

    void Epidemica::Simulation::Step()
    {
#ifdef _TRACE
        std::cout << "pass " << i << "..." << std::endl;
#endif
        m_hostsEpidemic.push_back(m_state->GetHostsEpidemic());
        m_processes->Execute(*m_state, Globals::GetTimeProfiler());
    }

    void Epidemica::Simulation::Finalize()
    {
        //TODO:
        //m_activeFactory->Finalize();
    }
}

std::ostream& Epidemica::operator<<(std::ostream& stm, const Epidemica::Simulation& a)
{
    return stm << "Simulation: " << std::endl
        << *a.m_processes << std::endl
        << *a.m_state << std::endl;
}
