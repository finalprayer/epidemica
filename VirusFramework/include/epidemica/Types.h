#pragma once

//#################################
// Types
//#################################
//This public header file is to be used by all other
// header files in this project for subsequent compile time
// types throughout the project

#include <array>
#include <set>
#include <vector>

#define MAX_NEIGHBORS 8
#define MAX_HOSTS 8


//=============================================
//Dynamically sized vector implementation
//=============================================
#define VECTOR_TYPE_STDVECTOR 1 //(std::vector)
#define VECTOR_TYPE_VALARRAY 2  //(std::valarray)
#define VECTOR_TYPE_UBLAS 3
#define VECTOR_TYPE VECTOR_TYPE_VALARRAY

#if VECTOR_TYPE == VECTOR_TYPE_STDVECTOR
#include <vector>
template<typename T>
using vector = std::vector<T>;
#elif VECTOR_TYPE == VECTOR_TYPE_VALARRAY
#include <valarray>
template<typename T>
using vector = std::valarray<T>;
#elif VECTOR_TYPE == VECTOR_TYPE_UBLAS
template<typename T>
using vector = boost::numeric::ublas::vector<T, std::vector<T>>;
#else
"Unknown Vector Type"
#endif


/**
* @struct	seir_vector
*
* @brief	Construction for SEIR vector from type.h, where type.h defines all vector based size,
* 			including SEIR, dynamic sized vector, data layout, data vector,
* 			and random engine.
*/

//=========================================
// SEIR Vector Implementation
//========================================
// Seir data structure is of a known data length and is
// one of the most flexible structures for vectorized maths
#define SEIR_LENGTH 4
#define SEIR_VECTOR_TYPE_STDARRAY 1
#define SEIR_VECTOR_TYPE_VALARRAY 2
#define SEIR_VECTOR_TYPE SEIR_VECTOR_TYPE_VALARRAY

#if SEIR_VECTOR_TYPE == SEIR_VECTOR_TYPE_STDARRAY
struct seir_vector
{
    std::array<int, SEIR_LENGTH> data;
    seir_vector() { data = {}; }
    int& operator[](std::size_t idx) { return data[idx]; }
    const int& operator[](std::size_t idx) const { return data[idx]; }
};
#elif SEIR_VECTOR_TYPE == SEIR_VECTOR_TYPE_VALARRAY
struct seir_vector
{
    std::valarray<int> data;
    seir_vector() { data = std::valarray<int>(SEIR_LENGTH); }
    int& operator[](std::size_t idx) { return data[idx]; }
    const int& operator[](std::size_t idx) const { return data[idx]; }
};
#else
"Unknown SEIR Vector type"
#endif

//=========================================
// Data Vector Type
//=========================================
//Data vector type is used for storing large amounts
//of data such as Locations and Hosts

#define DATA_VECTOR_TYPE_STDVECTOR 1
#define DATA_VECTOR_TYPE_STDARRAY 2
#define DATA_VECTOR_TYPE DATA_VECTOR_TYPE_STDVECTOR

#if DATA_VECTOR_TYPE == DATA_VECTOR_TYPE_STDARRAY
using neighbour_vector = std::array<int, MAX_NEIGHBORS>;
using host_set = std::array<int, MAX_HOSTS>;
#elif DATA_VECTOR_TYPE == DATA_VECTOR_TYPE_STDVECTOR
using neighbour_vector = std::vector<int>;
using host_set = std::set<int>;
#else
"Unknown data vector type"
#endif

//==================================
// Random Number Engine
//==================================
#include <random>
#define USE_MINSTD 1
#if USE_MINSTD
typedef std::minstd_rand random_engine;
#elif USE_MINSTD0
typedef std::minstd_rand0 random_engine;
#else
typedef std::default_random_engine random_engine;
#endif

//=======================================================
// Data Layout
//=======================================================
//locations and hosts data is stored in structures to
// allow cache optimsation when modifying single locations
// and hosts one at time.
#define DATA_LAYOUT_INTERLEAVED 1
//each property of locations and hosts data is store sequentially
// to allow cache optimsation when modify a single 
// property of all locations and hosts.
#define DATA_LAYOUT_SEQUENTIAL 2
//Data layout used by hosts and locations arrays
// Valid values are:
// INTERLEAVED, SEQUENTIAL
#define DATA_LAYOUT DATA_LAYOUT_INTERLEAVED

struct Host;
struct Location;
#if DATA_LAYOUT == DATA_LAYOUT_INTERLEAVED
typedef vector<Host> host_vector;
typedef vector<Location> location_vector;
#elif DATA_LAYOUT == DATA_LAYOUT_SEQUENTIAL
//Not implemented, processes typically traverse
// through hosts and locations and use multiple
// member variables. Sequential will require different
// for loops and large math library arrays. Theoretically
// practical but risky for development time and correctness.
#endif


//=========================
// Typdefs
//=========================
//#ifdef RAPIDJSON_FOUND
//#include <rapidjson/rapidjson.h>
//#include <rapidjson/document.h>
//typedef int integer
//#endif