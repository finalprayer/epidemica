#pragma once
#include <epidemica/state/VectorState.h>

#if BOOST_FOUND
#include <boost/property_tree/ptree.hpp>
#endif
#if RAPIDJSON_FOUND
#include <rapidjson/document.h>
using GenericObject = rapidjson::GenericObject<true, rapidjson::GenericValue<rapidjson::UTF8<char>>>;
#endif
#if MPI_FOUND
#include <mpi.h>
#endif

namespace Epidemica
{
    /**
     * @brief A host represents an agent/human that is suseptible to virus infection from vectors.
     */
    struct Host
    {
    public:
        int m_hostId;
        int m_locationId;
        InfectionState m_infectionState;
        int m_homeId;
        int m_hubId;

        /**
         * @brief Default constructor
         */
        Host();

        /**
         * @brief Constructor
         * @param hostId         Identifier for the host.
         * @param locationId     Identifier for the location.
         * @param infectionState State of the infection.
         * @param homeId         Identifier for the home.
         * @param hubId          Identifier for the hub.
         */
        Host(int hostId, int locationId, InfectionState infectionState, int homeId, int hubId);

        /**
         * @brief Destructor
         */
        ~Host();

#if BOOST_FOUND
        /**
         * @brief Constructor
         * @param hostId Identifier for the host.
         * @param host   The host.
         */
        Host(int hostId, const boost::property_tree::ptree& host);
#endif

#if RAPIDJSON_FOUND
        /**
         * @brief Constructor
         * @param hostId Identifier for the host.
         * @param json   The JSON object.
         */
        Host(int hostId, const GenericObject& json);
#endif

#if MPI_FOUND
    private:
        //Static variables creating and getting the MPI Datatype for transfering arrays
        // of structs over MPI
        static MPI_Datatype s_mpi_datatype;

        static bool s_mpi_initialized;

        /**
         * @brief Initializes the mpi datatype
         */
        static void InitMPIDatatype();

        /**
         * @brief Gets MPI Datatype
         * @return The mpi datatype.
         */
        static MPI_Datatype GetMPIDatatype();
#endif
    };
}