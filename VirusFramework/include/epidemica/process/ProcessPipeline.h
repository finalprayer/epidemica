#pragma once
#include <vector>
#include <epidemica/process/IProcess.h>
#include <epidemica/process/IProcessPipeline.h>

namespace Epidemica
{
    class IProcessFactory;
    class IProcess;
    struct SimulationState;
    class AbstractProcessFactory;
    class TimeProfiler;

    /**
     * \class ProcessPipeline
     * \author Callan
     * \date 26/03/2017
     * @brief A collection of process objects, each having implicit synchronisation points between each proccess.
     * It might be worthwhile structuring this to a Builder pattern. It might be necessary to crate implementation
     * specific versions of this via a simpler interface (i.e. IProcessPipeline).
     * Use a ProcessFactory to conveniently construct processes and pipelines.
     */
    class ProcessPipeline : public IProcessPipeline
    {
    private:
        //TODO: shared_ptr may be best, depending on whether factories want to keep reference to their processes
        //if not, may be best to use unique_ptr or simple value if copy constructor has no overhead
        // Note: You CANNOT store vectors of abstract classes by value, raw pointer seems to be the popular choice according
        // to the web, remember to dispose these at the end, or simply use a unique_ptr
        std::vector<std::shared_ptr<IProcess>> m_processes;

        //std::weak_ptr<AbstractProcessFactory> m_processFactory;

    public:
        ProcessPipeline();
        //TODO: may want to consider the consequences of copying the ownership by value
        ProcessPipeline(const std::vector<std::shared_ptr<IProcess>>& processes);
        ~ProcessPipeline();

        void Execute(SimulationState& state, TimeProfiler* profiler = nullptr);

        friend std::ostream& operator<<(std::ostream& stm, const ProcessPipeline &a);
    };
}

