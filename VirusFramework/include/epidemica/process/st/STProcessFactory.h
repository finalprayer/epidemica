#pragma once

#include "epidemica/process/AbstractProcessFactory.h"

namespace Epidemica
{
    class IProcess;

	/**
	 * @class	STProcessFactory
	 *
	 * @brief	the single thread Process Factory (or saying a pipeline) of arbo virus simulator.
	 * 			
	 * This is the place for you to extend process, re-order, add or remove processes to fit your model.
	 */

	class STProcessFactory : public AbstractProcessFactory
	{
	public:
		STProcessFactory();
		~STProcessFactory();

        static std::string tag() { return "ST"; }
        
        IProcess* CreateProcess(std::string processName) override;
        virtual IProcess* CreateInfectionInVectors() override;
        virtual IProcess* CreateInfectionInHosts() override;
        virtual IProcess* CreateRegularHumanMovement() override;
        virtual IProcess* CreateVectorMovement() override;
        virtual IProcess* CreateVectorInfection() override;
        virtual IProcess* CreateHostInfection() override;
        virtual IProcess* CreateVectorPopulation() override;
	};
}

