#pragma once
#include <epidemica/process/IProcess.h>

#include <epidemica/Types.h>
#include <random>

namespace Epidemica
{
    namespace ST
    {
        /**
         * @class	STHostInfection
         *
         * @brief	Vectors have a chance of infecting hosts at each location.
         *
         */

        class STHostInfection : public IProcess
        {
        private:
            random_engine m_generator;
        public:
            STHostInfection();
            ~STHostInfection();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}

