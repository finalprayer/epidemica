#pragma once
#include <epidemica/process/IProcess.h>


namespace Epidemica
{
    namespace ST
    {
        /**
         * @class	STRegularHumanMovement
         *
         * @brief	Regular Human Movement process, simulation humans moving from one place to other.
         */

        class STRegularHumanMovement : public IProcess
        {
        public:
            STRegularHumanMovement();
            ~STRegularHumanMovement();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}

