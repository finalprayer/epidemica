#pragma once
#ifdef OPENMP_FOUND
#include <epidemica/process/IProcess.h>

#include <epidemica/Types.h>
#include <random>

namespace Epidemica
{
    namespace OMP
    {        
        /**
         * @class	OMPHostInfection
         *
         * @brief	An OpenMP used host infection process.
         *
         */

        class OMPHostInfection : public IProcess
        {
        private:
            random_engine m_generator;
        public:
            OMPHostInfection();
            ~OMPHostInfection();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}
#endif