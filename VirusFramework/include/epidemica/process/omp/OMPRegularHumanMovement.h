#pragma once
#if OPENMP_FOUND
#include <epidemica/process/IProcess.h>


namespace Epidemica
{
    namespace OMP
    {	
        /**
         * @class	OMPRegularHumanMovement
         *
         * @brief	An OpenMP used regular human movement process.
         */

        class OMPRegularHumanMovement : public IProcess
        {
        public:
            OMPRegularHumanMovement();
            ~OMPRegularHumanMovement();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;
        };
    }
}

#endif