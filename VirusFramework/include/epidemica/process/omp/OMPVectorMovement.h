#pragma once
#if OPENMP_FOUND
#include <epidemica/Types.h>
#include <epidemica/process/IProcess.h>
#include <epidemica/state/VectorState.h>
#include <epidemica/state/SimulationState.h>
#include <valarray>
#include <random>

namespace Epidemica
{
    namespace OMP
    {
        /**
         * @class	OMPVectorMovement
         *
         * @brief	An OpenMP used vector movement process.
         */

        class OMPVectorMovement : public IProcess
        {
        private:
            std::default_random_engine m_generator;
        public:
            OMPVectorMovement();
            ~OMPVectorMovement();

            std::string GetName() const override;
            void Execute(SimulationState& state) override;

            /**
             * Returns a map of locationId : Moving SEIR_vector
             * TODO: Consider return neighbourId : Moving SIER_vector
             */
            std::valarray<seir_vector> DistributeVectors(Location& location, seir_vector movers);
        };
    }
}

#endif