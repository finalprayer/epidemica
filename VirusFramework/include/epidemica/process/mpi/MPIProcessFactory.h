#pragma once
#if MPI_FOUND

#include "epidemica/process/AbstractProcessFactory.h"

#include <mpi.h>

namespace Epidemica
{
	/**
	 * @class	MPIProcessFactory
	 *
	 * @brief	This is the constructor for MPI process. (Where you add, Edit or remove MPI process)
	 * 			
	 * 	From the time it sumitted, it construct Infection In Hosts and Infection In Vectors process,
	 * 	and you can implement more processes if you want to.
	 * 	
	 * 	Please refer to Maintainance Manual for guidelines of adding processes.
	 *
	 */

	class MPIProcessFactory : public AbstractProcessFactory
	{
	public:
		MPIProcessFactory();
		~MPIProcessFactory();
        
        static std::string tag() { return "MPI"; }
        
        ProcessPipeline* CreateProcessPipeline() override;
        IProcess* CreateProcess(std::string processName) override;
        
        virtual IProcess* CreateInfectionInVectors() override;
        virtual IProcess* CreateInfectionInHosts() override;
        virtual IProcess* CreateRegularHumanMovement() override;
        virtual IProcess* CreateVectorMovement() override;
        virtual IProcess* CreateVectorInfection() override;
        virtual IProcess* CreateHostInfection() override;
        virtual IProcess* CreateVectorPopulation() override;

        virtual void Initialize() override;
        virtual void Finalize() override;

        /**
          * MPI Specific
         **/
        static void InitDatatype();
	};
}
#endif