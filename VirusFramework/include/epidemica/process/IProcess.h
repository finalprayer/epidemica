#pragma once

namespace Epidemica
{
    struct SimulationState;

    /**
    * @class IProcess
    * @brief All operations that alter a simulation state should implement this.
    */
    class IProcess
    {
    public:
        /**
         * Default constructor
         */
        IProcess();

#if BOOST_FOUND
        #include <boost/property_tree/ptree.hpp>
        IProcess(boost::property_tree::ptree& properties);
#endif

        /**
         * Destructor
         */
        ~IProcess();

        /**
         * @brief Gets the name of the process.
         * @return The name.
         */
        virtual std::string GetName() const = 0;

        /**
         * @brief Executes the process on a given state.
         * @param [in,out] state The state.
         */
        virtual void Execute(SimulationState& state) = 0;
    };
	
	std::ostream& operator<<(std::ostream& stm, const IProcess& a);
}