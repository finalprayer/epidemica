#pragma once

#include "epidemica/process/AbstractProcessFactory.h"

namespace Epidemica
{
	/**
	 * @class	OMPIProcessFactory
	 *
	 * @brief	An OpenMP + MPI process factory. Which enables you to use both OpenMP and MPI within the process.
	 * 
	 * Nothing here has been implemented, you are free to implement this if you want it.
	 */

	class OMPIProcessFactory : public AbstractProcessFactory
	{
	public:
		OMPIProcessFactory();
		~OMPIProcessFactory();
        
        static std::string tag() { return "OMPI"; }
        
        IProcess* CreateProcess(std::string processName) override;
        virtual IProcess* CreateInfectionInVectors() override;
        virtual IProcess* CreateInfectionInHosts() override;
        virtual IProcess* CreateRegularHumanMovement() override;
        virtual IProcess* CreateVectorMovement() override;
        virtual IProcess* CreateVectorInfection() override;
        virtual IProcess* CreateHostInfection() override;
        virtual IProcess* CreateVectorPopulation() override;
	};
}