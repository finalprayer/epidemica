#include "stdafx.h"
#include "JsonHello.h"

#if BOOST_FOUND

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace pt = boost::property_tree;
    
void JsonHello::JsonDemo()
{
    pt::ptree root;
    std::string filename = "hello.json";
    try
    {
        pt::read_json(filename, root);
    }
    catch(pt::json_parser_error const& ex)
    {
        std::cerr << "failed to read : " << filename << std::endl;
        std::cerr << ex.message() << std::endl;
        return;
    }
    
    //serialize the tree directly as xml or json
    pt::write_json(std::cout, root);
    std::cout << std::endl;
    pt::write_xml(std::cout, root);
    std::cout << std::endl << std::endl;
    
    //access properties directly
    std::cout << "hello : " << root.get<std::string>("hello") << std::endl;
    
    //iterate through properties (not sure about multiple different elements)
    for(auto it = root.begin(); it != root.end(); ++it)
    {
        std::cout << it->first << " : " << it->second.get_value<std::string>() << std::endl;
    }
}

#endif

void JsonHello::RapidJsonDemo()
{
    /*
    std::ifstream t("hello.json");
    std::string str;
    str.assign(std::istreambuf_iterator<char>(t), std::istreambuf_iterator<char>());
    
    rapidjson::Document document; 
    document.Parse(str.c_str());
    if(document.HasParseError())
    {
        std::cout << "Error" << std::endl;
    }
    */
}
