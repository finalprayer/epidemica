
#if BOOST_FOUND

#include "stdafx.h"
#include "ParallelismHello.h"
#include "JsonHello.h"
#include "OpenCLHello.h"

#include <boost/program_options.hpp>

namespace po = boost::program_options;

int main(int argc, char **argv)
{
    std::cout << "Hello World!" << std::endl;

    po::options_description desc{"Options"};
    desc.add_options()
        ("help,h",          "Help screen")
        ("opencl",          "OpenCL Info")
        ("ompmpi",          "OpenMP and MPI")
        ("json",            "JSON Demo");
    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    bool actionfound = false;
    if(vm.count("help"))
    {
        std::cout << desc << std::endl;
        actionfound = true;
    }
    if(vm.count("cl"))
    {
#if OPENCL_FOUND
        OpenCLHello::OpenCLDemo();
        actionfound = true;
#else
		std::cout << "opencl not built" << std::endl;
#endif
    }
    if(vm.count("json"))
    {
        JsonHello::JsonDemo();
        actionfound = true;
    }
    if (vm.count("ompmpi"))
    {
#if MPI_FOUND
        ParallelismHello::OpenMP_MPI_Demo();
        actionfound = true;
#endif
    }

    if (!actionfound)
    {
        std::cout << desc << std::endl;
    }
    
    return 0;
}

#else
int main(int argc, char **argv)
{
    return 0;
}
#endif