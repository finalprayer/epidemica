cmake_minimum_required(VERSION 3.2 FATAL_ERROR)

#already included
include(../cmake/AutoSourceGroup.cmake)
include(../cmake/PrecompiledHeader.cmake)

set(TARGET_NAME VirusSimulator)

include_directories(
    .
    pch
    include/epidemica
	../third_party
)

#TODO: this is probably not the best for copying resources, maybe
# consider making an asset project.

file(
	GLOB_RECURSE CXX_SRCS
	LIST_DIRECTORIES false
	RELATIVE ${CMAKE_CURRENT_LIST_DIR}
	"${CMAKE_CURRENT_LIST_DIR}/*.cpp"
)

file(
	GLOB_RECURSE CXX_HEADERS
	LIST_DIRECTORIES false
	RELATIVE ${CMAKE_CURRENT_LIST_DIR}
	"${CMAKE_CURRENT_LIST_DIR}/*.h"
)

file(
	GLOB_RECURSE RESOURCE_FILES
	LIST_DIRECTORIES false
	RELATIVE ${CMAKE_CURRENT_LIST_DIR}
	"${CMAKE_CURRENT_LIST_DIR}/scenarios/*"
)
set(RESOURCE_FILES "${RESOURCE_FILES};taskconfig.json")
set(RESOURCE_FILES "${RESOURCE_FILES};log.conf")


#=========================
# Executable Configuration
#=========================
add_executable (
	${TARGET_NAME}
	${CXX_SRCS}
	${CXX_HEADERS}
#	${RESOURCE_FILES} # this line may slow down certain IDEs
)

include_directories(
    .
	pch
	include
	include/epidemica
	${CMAKE_CURRENT_LIST_DIR}/../VirusFramework/include)

set(LINK_OPTIONS VirusFramework;${LINK_OPTIONS})

#=============================
# Add filters for IDEs
#=============================
auto_filter_group("Header Files" ${CXX_HEADERS})
auto_filter_group("Source Files" ${CXX_SRCS})
auto_filter_group("Resource Files" ${RESOURCE_FILES})

#======================================
#Post build step for copying resources
#======================================
#https://stackoverflow.com/questions/14368919/cmake-custom-command-copy-multiple-files

#TODO: for some reason this must come first
add_custom_command(
	TARGET ${TARGET_NAME} POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E copy_if_different
	${CMAKE_CURRENT_LIST_DIR}/taskconfig.json
	$<TARGET_FILE_DIR:${TARGET_NAME}>)

add_custom_command(
	TARGET ${TARGET_NAME} POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E copy_if_different
	${CMAKE_CURRENT_LIST_DIR}/log.conf
	$<TARGET_FILE_DIR:${TARGET_NAME}>)


if(WIN32)
add_custom_command( # On post windows vista machines, robocopy is the most modern copy tool
	TARGET ${TARGET_NAME} POST_BUILD
	COMMAND (robocopy /e /ns /nc /nfl /ndl /njh /njs
	${CMAKE_CURRENT_LIST_DIR}/scenarios
	$<TARGET_FILE_DIR:${TARGET_NAME}>/scenarios)
	^& IF %ERRORLEVEL% LEQ 1 exit 0
	^& IF %ERRORLEVEL% LEQ 2 exit 0)
else()
add_custom_command( # On Unix-based systems cp is standard
	TARGET ${TARGET_NAME} POST_BUILD
	COMMAND cp -r -u
	${CMAKE_CURRENT_LIST_DIR}/scenarios
	$<TARGET_FILE_DIR:${TARGET_NAME}>/scenarios)
endif()




#===================
# Build Targets
#===================
target_include_directories(${TARGET_NAME} PUBLIC ${INCLUDE_DIRECTORIES})
target_link_libraries(${TARGET_NAME} ${LINK_OPTIONS})
set_property(TARGET ${TARGET_NAME} PROPERTY FOLDER "executables")

set_target_properties(${TARGET_NAME} PROPERTIES
                      #VS_DEBUGGER_WORKING_DIRECTORY "$(TargetDir)"
                      #RESOURCE "${RESOURCE_FILES}" # only OSX bundle macro used this property
                      RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)

#===================
# Precompiled Header
#===================
# from https://github.com/larsch/cmake-precompiled-header/blob/master/example/CMakeLists.txt
# could alternatively rename "stdafx" to "pch"
if(MSVC AND NOT ${CMAKE_GENERATOR} STREQUAL "NMake Makefiles")
	#for some reason not working on my linux - Callan
	add_precompiled_header(
		${TARGET_NAME} 
		"pch/stdafx.h"
		FORCEINCLUDE
	    SOURCE_CXX "src/stdafx.cpp"
		)
endif()

#======================
# Visual Studio Tweaks
#======================
# Adds logic to INSTALL.vcproj to copy app.exe to destination directory
install (TARGETS ${TARGET_NAME}
         RUNTIME DESTINATION ${PROJECT_SOURCE_DIR}/_install)


if(MSVC) # Set Local Debugger Working Directory
file( WRITE "${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}.vcxproj.user"
"<?xml version=\"1.0\" encoding=\"utf-8\"?>
<Project ToolsVersion=\"15.0\" xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\">
	<PropertyGroup>
	<LocalDebuggerWorkingDirectory>$(OutputPath)</LocalDebuggerWorkingDirectory>
	<DebuggerFlavor>WindowsLocalDebugger</DebuggerFlavor>
	</PropertyGroup>
</Project>"
)
endif()

