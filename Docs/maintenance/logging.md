# 4.3 Logging Modification and Usage {#logging}
## Introduction
This project uses a lightweight single header C++ logging library called Easylogging++ to build its own logging system. All the detailed information about this library and the source code can be found on [GitHub Easylogging++][easyloggingpp]. 

You can also find the full document in the project directory `third_party/easyloggingpp/README.md`

  - Easylogging++ allows you to change everything in the program dynamically or use a configuration file to customise your log file before you run the program.
## How to use it in this project

  If you want to customise your own log format and you don't want to change the code, the easiest way to do it is using a configuration file. 
  The default output (which means you don't provide a configuration file for the simulator) of will be look like this:
```sh
2017-11-02 16:05:37,077 INFO [default] i, 1, 56464, 42869
2017-11-02 16:05:37,077 INFO [default] r, 1, 56469, 31271
```
A sample configuration file named `log.conf` was included in the root directory of the project. The output of the simulator when we use this configuration file shows below:
```sh
r, 3, 52689, 53336
r, 3, 52760, 37561
i, 3, 52890, 56958
```
The detailed information about how to set up the configuration file can be found on [GitHub Easylogging++][easyloggingpp] or in project directory `third_party/easyloggingpp/README.md`

  - After customise the configuration file, make sure put it in the same directory as the excutable program, in this project the default excutable file will be in `builds/bin/` after you build the project.
  - The single core version will generate one log file called `node0.log`.
  - If you run the mpi version, it will generate the same number of files with different simulation result from different cores. The log file name will be 
`node` + `core index` + `.log`
For example, the command above will generate 4 different file named `node0.log`, `node1.log`, `node2.log`, `node3.log`

   [easyloggingpp]:<https://github.com/muflihun/easyloggingpp/blob/master/README.md>
