# 4.5 Optimisations Notes {#optimisation}

Below contains a list of optimisations performed on the simulator in the conversion from python to C/C++.

## 4.5.1 Complexities

### Python

| Process              | Complexity    |  RNG Calls   |
| -------------------  |:-------------:| ------------:|
| Host Infection       | O(H)          | H * binomial |
| Human Movement       | O(H)          |         none |
| Infection In Hosts   | O(H)          | H * binomial |
| Infection In Vectors | O(L)          | L * binomial |
| Vector Infection     | O(L)          | L * binomial |
| Vector Movement      | O(L*V)        | L * binomial + V * uniform |
| Vector Population    | O(L)          | L * (binomial + poisson) |

\* H: Hosts , L: Locations , V: vectors

The overall complexities of the single threaded python are reasonable for the data structures given and are using suitable distributions for random events.

The major outlier hower is in Vector movement, where a binomial distrubition sample is being called per vector.

## 4.5.2 Arbovirus Single Threaded

| Process              | Complexity    |  RNG Calls   |
| -------------------  |:-------------:| ------------:|
| Host Infection       | O(H)          | H * binomial |
| Human Movement       | O(H)          |         none |
| Infection In Hosts   | O(H)          | H * binomial |
| Infection In Vectors | O(L)          | L * binomial |
| Vector Infection     | O(L)          | L * binomial |
| Vector Movement      | O(L*N)        | L * binomial + N * uniform |
| Vector Population    | O(L)          | L * (binomial + poisson) |

\* H: Hosts , L: Locations , V: vectors , N: neighbours

From the original code the major change performed here was the change in
the number of uniform samples being perform. This implementation change has a small chance of changing the overall diffusal probability as this particular calculation proved to be difficult due to fact that there is an upper and lower sample limit of how many vectors can stay or move.

## 4.5.3 Arbovirus OpenMP Threaded

| Process              | Complexity     |  RNG Calls   |
| -------------------  |:--------------:| ------------:|
| Host Infection       | O(H) / cores   | H * binomial |
| Human Movement       | O(H)           |         none |
| Infection In Hosts   | O(H) / cores   | H * binomial |
| Infection In Vectors | O(L) / cores   | L * binomial |
| Vector Infection     | O(L) / cores   | L * binomial |
| Vector Movement      | O(L*N) / cores | L * binomial + N * uniform |
| Vector Population    | O(L) / cores   | L * (binomial + poisson) |

\* H: Hosts , L: Locations , V: vectors , N: neighbours

Compared to the Single Threaded code, nearly all processes have received near optimal performance increase for the available number of CPU cores. Much of this was due to almost embarassingly parallel nature that each process iterates either the hosts or locations collections.

The challenge that is faced however was in optimising human movement. This is due to the need to modify the hosts set used by the locations struct for quickly identifying which hosts are at any location at a given time.

This could potentially be improved by:

- performing the insert and erase operations with mutexes/atomic operations
- performing the insert and erase operations in a single threaded at the end.

For simple human movement, there is really not enough calculations happening to warrant a second iteration, but is an optimsation to consider should hosts become more agent like.

## 4.5.4 C/C++ Optimisation Approaches

### Reduce Random Number Generator Calls

A key aspect of the virus simulator is in how the random number generator is called. While the C++ standard library offers a number of powerful distribution and generator classes, getting the most out of these implementations still requires careful planning of using a distribution function wherever possible.

A particularly challenging instance of this usage is in VectorMovement where mosquitos have an associated probability of moving every cycle and a uniform chance of moving in each direction. A key optimisation here was to avoid using bernoulli trials per vector and instead use two distribution functions.

### Constigious Memory Allocation

Cache optimisation requires that data is loaded into memory for as long as possible before have to wait for cache registers to load a different section of memory.

One of the key improvements that C/C++ has over other languages is controlling where memory is allocated. This is highly important when dealing with arrays of data and dictionary objects.

One of the key improvements we have made is for hosts to be stored within an large struct array allocation and store their id, rather than use a dictionary/map object.

While this approach is less flexible than a dictionary, it can be extended to the memory pool concept, where a lookup table will allow fast indexing to hosts within an array if they are unsorted, but the memory pool can provide an iterator that will iterate in the order they exists in memory and reap the benefits of cache optimisation.

To reduce time spent on this optimisation, we have made the modification that all hosts shall have ids that match their location within an array, however a lookup table may be required to adapt for changing hot populations.

Another minor optimisation in this category is the use of fixed size arrays for SEIR data, which provides the same permance benefit of declaring 4 member variables in succession.

### Cache Optimised Data Layout

Cache optimised calculations involves performing operations on data located within cache memory for as long as possible. This means it is important to layout the data in a way that reflects how processes modify the state.

All existing process follow a pattern where they either iterate through hosts, or iterate through locations, and usually checking and modying a few things for each before moving to the next.

This kind of access to data suggests that the attributes of each host and location should be interleaved in memory, e.g.

Id1 x1 y1 S1 E1 I1 R1 Cap1 | Id2 x2 y2 S2 E2 I2 R2 Cap1 | Id3 x3 y3 S3 E3 I3 R3 Cap3

The alternative approach to this is by laying out the state by storing each attribute sequentially in memory, e.g.

Id1 id2 Id3 | S1 S2 S3 | E1 E2 E3 | I1 I2 I3 | R1 R2 R3 | Cap1 Cap2 Cap3

This can be beneficial for infection processes where only E and I attributes are being modified, but not worth the risk during project development.

### Reduce Conditional Branching

Branching via the use of if statements introduces a small overhead in execution on x86 processors as the processor may not have the next instruction already within its lowest level register, resulting a cache miss. There are only very few instances where this could be worked around in parts of the code, and compiler optimisation levels do check for this.

An extreme case of how this can be utilised in these situations is in sorting arrays of data before running a conditional statement, which allows the compiler to get only get a single cache miss after the if statement for the entire array.

### Minimize the number of virtual calls

Similar to conditional branching, virtual methods requires a memory jump when being executed as the compiler does not know until runtime which virtual method implementation is about to be run.

In this project the pipeline objects are in fact using virtual method calls, however the frequency that each process virtual call is being made is no where near the number of calls within each process. Where possible inside processes themselves, avoid making virtual calls and consider instead using templated methods which do not incur this overhead due to the template type being known at compile time. This can be seen in the probability distributions where templates are used for defining single or double precision.

### Inlining

Inlining is a C/C++ compiler feature where instead of a function calling a subroutine to a different section of memory, the compiler implements the function inline where it is being called from. This can be enforced via the inline keyword, or performed automatically with compiler optimisations.

### Build Configurations

Not really an optimisation, but the balance between reliability and performance can be difficult to manage in many projects. One advantage of the C/C++ compiler is the ability to utilise a debug configuration for checking data for simple errors, and then remove data checks and turn on compiler optimisations in a release configuration.