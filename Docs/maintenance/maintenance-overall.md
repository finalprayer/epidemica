# 4 Maintenance Manual {#maintenance-overall}
This project has been designed for extensive usage. In particular, this project
enables future developers to order, add or remove any process within the process
pipeline, together enables further implementations or add in new process pipeline.

All subsections include the architecture, open source system development strategy
and majority packages usage.