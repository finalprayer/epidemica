
### Tests

* Check Loading scenarios
	* JSONSimualtionScenario.h
	* possibly also PTreeSimulationScenario.h

* Check simulation can run successfully
	* Test ST factory tag
	* Test OMP factory tag
	* Check that running the simulator doesnt crash
	
* Check that the final simulation state is valid
	* Check that host IDs 1->n still exists
	* Check that all the points on the epidemic curve add up to the number of hosts
	
* Check building without optional libraries?
	* Possbily not possible with unit tests
	* test with and without:
		* python !!!
		* openmp !!
		* mpi !!!
		* mkl
		* opencl