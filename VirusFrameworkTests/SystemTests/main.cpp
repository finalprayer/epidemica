
#include <gtest/gtest.h>
#include <easylogging++.h>

#if MPI_FOUND
#include <mpi.h>
#endif

INITIALIZE_EASYLOGGINGPP
int main(int argc, char **argv)
{
#if MPI_FOUND
    MPI_Init(&argc, &argv);
#endif

    el::Configurations conf("log.conf");
    el::Loggers::reconfigureAllLoggers(conf);
	testing::InitGoogleTest(&argc, argv);

	int result = RUN_ALL_TESTS();
    
#if MPI_FOUND
    result |= MPI_Finalize();
#endif

    return result;
}
